var user = require("./db_connection");
const User = user.User;
const express = require("express");
const router = express.Router();
var crypto = require('crypto');

var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};

function saltHashPassword(userpassword) {
    var salt = genRandomString(16); /** Gives us salt of length 16 */
    var passwordData = sha512(userpassword, salt);
    return passwordData;
    // console.log('UserPassword = '+userpassword);
    // console.log('Passwordhash = '+passwordData.passwordHash);
    // console.log('\nSalt = '+passwordData.salt);
}

router.route("/:id").get(async (req, res) => {
  //console.log(req.params)
  let data = await User.find({ _id: req.params.id });
  res.json(data);
});

router
  .route("/")
  .get(async (req, res) => {
    let data = await User.find();
    res.json(data);
  })

  .post(async (req, res) => {
    // console.log(req.body);

    var salthash_password = saltHashPassword(req.body.password);
    console.log(salthash_password.passwordHash)

    data = {
        "f_name": req.body.f_name,
        "l_name": req.body.l_name,
        "email": req.body.email,
        "password": salthash_password.passwordHash,
        "mobile":req.body.mobile,
        "address": req.body.address
    }
    let u1 = new User(data);
    let result = await u1.save();
    res.json(result);
  })

  .put(async (req, res) => {
    var salthash_password = saltHashPassword(req.body.password);
    console.log(salthash_password.passwordHash)

    data = {
        
    }

    let u_data = await User.updateOne(
      { _id: req.body.id },
      {
        $set: {
            f_name : req.body.f_name,
            l_name : req.body.l_name,
            email : req.body.email,
            password : salthash_password.passwordHash,
            mobile :req.body.mobile,
            address : req.body.address
        },
      }
    );
    res.json(u_data);
  })

  .delete(async (req, res) => {
    let d_rec = await User.deleteOne({ _id: req.query.id });
    res.json(d_rec);
  });

module.exports = router;
