const mongoose = require("mongoose");

// const conn_str = "mongodb://localhost:27017/users"
const conn_str = "mongodb+srv://root:root@cluster0.8i0tm.mongodb.net/test?retryWrites=true&w=majority"
mongoose.connect(conn_str,  { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => console.log("connected sucessfully"))
.catch((err) => console.log(err));

//schema
const userSchema = new mongoose.Schema({
    f_name: String,
    l_name: String,
    email: String,
    password: String,
    mobile: Number,
    address: String,
})

//create collection
const User = new mongoose.model("User", userSchema);

exports.User = User;